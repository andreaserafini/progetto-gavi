# -*- coding: utf-8 -*-
"""
Created on Thu Mar  5 19:28:15 2020

@author: Andrea
"""
import random

def get_links(S, last_iter, page):
    
    toRet = list()
    for k in S.keys():
        if page in S[k]:
            toRet.append(last_iter[k] / len(S[k]))
            
    return toRet

def adjacency_pagerank(S, reverseS, alpha=0.75, tol=1.0e-8, max_iter=100):
    
    
    dict_pesi = dict.fromkeys(S, 1 / len(S.keys()))
    
    
    for i in range(max_iter):
#        print("Iter")
        last_iter = dict_pesi.copy()
#        print(3)
        for p in dict_pesi.keys():
            link_entranti = [last_iter[n]/len(S[n]) for n in reverseS[p] if len(S[n]) != 0]
            if len(link_entranti) != 0:
                dict_pesi[p] = alpha * (sum(link_entranti)) + (1 - alpha) / len(S.keys())
            else:
                dict_pesi[p] = (1 - alpha) / len(S.keys())
            print("Finito nodo", p)
        
        c = 1 / sum([dict_pesi[p] for p in dict_pesi.keys()])
        dict_pesi = dict([(k,dict_pesi[k] * c) for k in dict_pesi.keys()])
        
        print("Finito di normalizzare")
        
        err = sum([abs(dict_pesi[k] - last_iter[k]) for k in dict_pesi.keys()])
        print("Err ->", err)
        if err < len(dict_pesi.keys()) * tol:
            print(i)
            return dict_pesi
    
    return None
    
    
    